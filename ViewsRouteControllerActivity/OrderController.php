<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function display($CustomerID,$Name,$Address)
    {
        return view('customer',['CustomerID'=>$CustomerID, 'Name'=>$Name, 'Address'=>$Address]);
        
    }
    public function display1($ItemNo,$Name,$Price)
    {
    
        return view('item',['ItemNo'=>$ItemNo, 'Name'=>$Name, 'Price'=>$Price]);
       
    }
    public function display2($CustomerID,$Name,$OrderNo,$Date)
    {

        return view('order', ['CustomerID'=>$CustomerID, 'Name'=>$Name, 'OrderNo'=>$OrderNo, 'Date'=>$Date]);
    
    }
    public function display3($TransNo,$OrderNo,$ItemID,$Name,$Price,$Qty)
    {
    
        return view('orderDetails', ['TransNo'=>$TransNo, 'OrderNo'=>$OrderNo, 'ItemID'=>$ItemID, 'Name'=>$Name, 'Price'=>$Price, 'Qty'=>$Qty]);
    
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
