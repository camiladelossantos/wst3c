<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <title>Customer</title>
</head>
<body>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container">
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse justify-content-center" id="navbarSupportedContent">
        <ul class="navbar-nav mb-2 mb-lg-0">
            <li class="navbar-brand"><a class="nav-link active" aria-current="page" href="/customer">Customer</a></li>
            <li class="navbar-brand"><a class="nav-link" aria-current="page" href="/item">Item</a></li>
            <li class="navbar-brand"><a class="nav-link" aria-current="page"href="/order">Order</a></li>
            <li class="navbar-brand"><a class="nav-link" aria-current="page"href="/orderDetails">Order Details</a></li>
        </ul>
        </div>
    </div>
    </nav>


<div class="mx-auto" style = "max-width: 55rem; margin-top: 45px;">
    <div class="card border-dark " style=" background-color:">
    <div class="card-body">


    <div class="row"style="margin-left:30px; margin-right: 30px;">
        <label for="CustomerID" class="col-sm-2 col-form-label">Customer ID:</label>
    <div class="col-sm-10">
        <input type="text" readonly class="form-control" id="CustomerID" value="<?php echo"{$CustomerID}"?>">
    </div>
    </div>
<br>

<div class="row"style="margin-left:30px; margin-right: 30px;">
        <label for="Name" class="col-sm-2 col-form-label">Name:</label>
    <div class="col-sm-10">
        <input type="text" readonly class="form-control" id="Name" value="<?php echo"{$Name}"?>">
    </div>
    </div>
<br>

<div class="row"style="margin-left:30px; margin-right: 30px;">
        <label for="Address" class="col-sm-2 col-form-label">Address:</label>
    <div class="col-sm-10">
        <input type="text" readonly class="form-control" id="Address" value="<?php echo"{$Address}"?>">
    </div>
    </div>
<br>

    
</div>
</div>
</div>
<br>
<center>
<h3>Camila C. Delos Santos BSIT - 3C</h3>
</center>
</body>
</html>