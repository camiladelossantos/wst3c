<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\OrderController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/customer', function () {
    return view('customer');
});
    Route::get('/item', function () {
        return view('item');
    });
        Route::get('/order', function () {
            return view('order');
        });
            Route::get('/orderDetails', function () {
                return view('orderDetails');
            });

    Route::get('/customer/{CustomerID}/{Name}/{Address}', [OrderController::class, 'display']);
    Route::get('/item/{ItemNo}/{Name}/{Price}', [OrderController::class, 'display1']);
    Route::get('/order/{CustomerID}/{Name}/{OrderNo}/{Date}', [OrderController::class, 'display2']);
    Route::get('/orderDetails/{TransNo}/{OrderNo}/{ItemID}/{Name}/{Price}/{Qty}', [OrderController::class, 'display3']);