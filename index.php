<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <title>Index</title>
</head>
<body>
<div class="mx-auto" style="max-width: 30rem; margin-top: 100px;">
    <div class="card text-dark bg-light">
        <div class="card-header">
        About Me
        </div>
        <div class="card-body">
        Name: Camila C. Delos Santos</br>
        Year and Section: BSIT 3C</br>
        Subject: Elective 1 (Web Systems and Technologies 2)</br>
        <?php
        $offset= strtotime("+7 hours"); 
        $date = date("m-d-Y H:i:sa",$offset);
        echo "Date and Time: ". $date . "</br>";
        ?>
        </div>
    </div>
</div>
</body>
</html>