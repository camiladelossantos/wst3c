<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <title>Contact Me</title>
    <style>
        body{
            color: #ECE5D3;
            background:black;
        }
    </style>
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container">
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse justify-content-center" id="navbarSupportedContent">
        <ul class="navbar-nav mb-2 mb-lg-0">
            <li class="navbar-brand"><a class="nav-link" aria-current="page" href="/index">Home</a></li>
            <li class="navbar-brand"><a class="nav-link" aria-current="page" href="/about">About Me</a></li>
            <li class="navbar-brand"><a class="nav-link" aria-current="page"href="/registration">Registration</a></li>
            <li class="navbar-brand"><a class="nav-link" aria-current="page"href="/login">Log In</a></li>
             <li class="navbar-brand"><a class="nav-link active" aria-current="page"href="/contact">Contact Me</a></li>
        </ul>
        </div>
    </div>
    </nav>
    <br>
    <div class="mx-auto" style="max-width:50rem; margin-top: 45px;">
    <div style=" background-color: #1F1F1E">
    <div class="row g-0">

      <div class="col-md-4">
          <img src = "{{ URL('images/pic3.jpg') }}" height = "420" width = "255" style="margin-left: 80px; margin-top: 50px; border-radius: 50px" >
          <br><br>
      </div>
      
      <div class="col-md-8">
      <div class="card-body" style=" margin-left: 19px; margin-top: 40px;">
      <div class="mt-2" style=" color: #ECE5D3;">

        <h1 class="card-title" style="margin-left: 130px;">Contact Me</h1>
        <br><br>
        <div class=" text-center " style="margin-left: -10px;">
        <ul class="list-unstyled mb-0 ">
            <li><img src = "{{ URL('images/location.png') }}" height = "30" width = "25"></li>
            <p class="card-text">Barangay Consolacion, Urdaneta City, Pangasinan</p>
            <br>
            <li> <img src = "{{ URL('images/telephone.png') }}" height = "30" width = "25"></li>
            <p class="card-text">0915 646 9752</p>
             <br>
            <li> <img src = "{{ URL('images/email.png') }}" height = "40" width = "35"></li>
            <p class="card-text">camila00018@gmail.com</p>
        </ul>
        </div>
        </div>
        </div>
        </div>
        </div>

        <hr class="d-print-none" style="margin-left: 35px; margin-right: 35px;"/>
        <div class="contant-section px-3 px-lg-4 pb-4"><br>
          <h5 style="color: #ECE5D3; margin-left: 30px">For all inquiries, please get in touch with me using the form below.</h5>
          <br>

          <div class="my-2"  style="margin-left: 30px; margin-right: 30px;">
          <div class="row">
          <div class="col-6">
            <input class="form-control" type="text" id="name" name="name" placeholder="Your Name" required>
          </div>

          <div class="col-6">
            <input class="form-control" type="email" id="email" name="_replyto" placeholder="Your E-mail" required>
          </div>
          </div>
          <br>

          <div class="form-group my-2">
            <textarea class="form-control" style="resize: none;" id="message" name="message" rows="4"  placeholder="Your Message" required></textarea>
          </div>
          <br><br>

          <center>
            <a class="btn btn-info" style=" width: 130px; height: 41px;" href="/about"><b>Submit</b></a>
          </center>

          <br>
        </div>
      </div>
    </div>
    </div>
    <br><br>
</body>
</html>