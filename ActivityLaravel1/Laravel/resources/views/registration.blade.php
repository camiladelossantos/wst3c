<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <title>Registration</title>
    <style>
        body{
            color: #ECE5D3;
            background:black;
        }
    </style>
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container">
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse justify-content-center" id="navbarSupportedContent">
        <ul class="navbar-nav mb-2 mb-lg-0">
            <li class="navbar-brand"><a class="nav-link" aria-current="page" href="/index">Home</a></li>
            <li class="navbar-brand"><a class="nav-link" aria-current="page" href="/about">About Me</a></li>
            <li class="navbar-brand"><a class="nav-link active" aria-current="page"href="/registration">Registration</a></li>
            <li class="navbar-brand"><a class="nav-link" aria-current="page"href="/login">Log In</a></li>
             <li class="navbar-brand"><a class="nav-link" aria-current="page"href="/contact">Contact Me</a></li>
        </ul>
        </div>
    </div>
    </nav>
    <br>
    <div class="mx-auto" style = "max-width: 50rem; margin-top: 45px;">
    <div class="card border-dark " style=" background-color: #1F1F1E; border-radius: 55px;">
    <div class="card-body">

        <br><br>
            <center><h1>Register</h1></center>
        <br><br>

        <div class="row" style="margin-left:30px; margin-right: 30px;">
        <label for="colFormLabel" class="col-sm-2 col-form-label">Firstname:</label>
            <div class="col-sm-10">
                 <input type="text" class="form-control" id="colFormLabel" placeholder="firstname">
            </div>
        </div>

        <br>
        <div class="row" style="margin-left:30px; margin-right: 30px;">
        <label for="colFormLabel" class="col-sm-2 col-form-label">Lastname:</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="colFormLabel" placeholder="lastname">
            </div>
        </div>
        <br>

        <div class="row" style="margin-left:30px; margin-right: 30px;">
        <label for="colFormLabel" class="col-sm-2 col-form-label">Birthday:</label>
            <div class="col-sm-10">
                <input type="date" class="form-control" id="colFormLabel" placeholder="dd/mm/yyyy">
            </div>
        </div>
        <br>

        <div class="row" style="margin-left:30px; margin-right: 30px;">
        <label class="col-sm-2 col-form-label">Town:</label> 
        <div class="col-sm-10">
            <select name="town" class="form-select">
                <option value = "">select</option>
                <option value="Nancayasan">Nancayasan</option>
                <option value="Camanang">Camanang</option> 
                <option value="Consolacion">Consolacion</option>
                <option value="Macalong">Macalong</option> 
                <option value="Sugcong">Sugcong</option>
            </select>
        </div>
        </div>
        <br><br>

        <center>
            <a class="btn btn-info" style=" width: 130px; height: 41px;" href="/login"><b>Submit</b></a>
        </center>
        <br><br>

    </div>
    </div>
    </div>
    <br><br>
</body>
</html>