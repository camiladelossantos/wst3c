<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <title>Home</title>
    <style>
        body{
            color: #ECE5D3;
            background:black;
        }
    </style>
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container">
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse justify-content-center" id="navbarSupportedContent">
        <ul class="navbar-nav mb-2 mb-lg-0">
            <li class="navbar-brand"><a class="nav-link active" aria-current="page" href="/index">Home</a></li>
            <li class="navbar-brand"><a class="nav-link" aria-current="page" href="/about">About Me</a></li>
            <li class="navbar-brand"><a class="nav-link" aria-current="page"href="/registration">Registration</a></li>
            <li class="navbar-brand"><a class="nav-link" aria-current="page"href="/login">Log In</a></li>
              <li class="navbar-brand"><a class="nav-link" aria-current="page"href="/contact">Contact Me</a></li>
        </ul>
        </div>
    </div>
    </nav>
    <br>
    <div class="mx-auto" style = "max-width: 50rem; margin-top: 45px;">
    <div class="card border-dark " style=" background-color: #1F1F1E;">
    <div class="card-body">
        
    <div class="col-md-4">
          <img src = "{{ URL('images/pic6.jpg') }}" height = "300" width = "400" style="margin-left: 182px; margin-top: 50px; border-radius: 50px" >
          <br><br>
      </div>

        <br>
        <center>
            <h1>Hello, I'm Camila!</h1>
        </center>

        <br>
        <center>
            <h4>"Life is beautiful not because of the things we see or do. <br>Life is beautiful because of the people we meet."</h4>
        </center>

        <br><br>
        <center>
            <h5>Let's Get To Know Each Other</h5>
        <center>

        <br><br>
        <center><a class="btn btn-info" style=" width: 130px; height: 41px;" href="/about"><b>About Me</b></a><center>
        <br><br>

    </div>
    </div>
    </div>
    <br><br>
</body>
</html>