<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <title>About Me</title>
    <style>
        body{
            color: #ECE5D3;
            background:black;
        }
    </style>
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container">
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse justify-content-center" id="navbarSupportedContent">
        <ul class="navbar-nav mb-2 mb-lg-0">
            <li class="navbar-brand"><a class="nav-link" aria-current="page" href="/index">Home</a></li>
            <li class="navbar-brand"><a class="nav-link active" aria-current="page" href="/about">About Me</a></li>
            <li class="navbar-brand"><a class="nav-link" aria-current="page"href="/registration">Registration</a></li>
            <li class="navbar-brand"><a class="nav-link" aria-current="page"href="/login">Log In</a></li>
              <li class="navbar-brand"><a class="nav-link" aria-current="page"href="/contact">Contact Me</a></li>
        </ul>
        </div>
    </div>
    </nav>
    <br><br><br>
    <div class="container">
    <div class="mx-auto" style=" background-color: #1F1F1E; max-width: 61rem; " >
      <div class="cover-bg p-3 p-lg-4">

        <div class="row">
          <div class="col-lg-3 col-md-4">
            <div class="avatar hover-effect">
              <img src="images/pic1.jpg" height = "420" width = "255" style=" border-radius: 50px; margin-left: 25px; margin-top: 30px"/>
            </div>
          </div>
      
          <div class="col-lg-8 col-md-7"  style="margin-left: 70px; margin-top: 10px;">
          <br>
          <h1>About Me</h1>
       
            <p>Hello, my name is Camila C. Delos Santos, and I am an extremely driven person with <br>a clear aim of success. 
              Throughout a difficult stage of development, I was able to maintain a consistent approach. 
              I am someone on whom you can rely. 
              Dancing has always been a talent of mine since I was a child. To unwind, I also enjoy eating and watching anime.</p>
          <br>
          <h1>Basic Information</h1>
          <div class="row mt-2">
          <div class="col-sm-4">
            <div class="pb-1">Age</div>
          </div>

          <div class="col-sm-8">
            <div class="pb-1">20</div>
          </div>

          <div class="col-sm-4">
            <div class="pb-1">Email</div>
          </div>

          <div class="col-sm-8">
            <div class="pb-1">camila00018@gmail.com</div>
          </div>
          
          <div class="col-sm-4">
            <div class="pb-1">Phone</div>
          </div>

          <div class="col-sm-8">
            <div class="pb-1">0915 646 9752</div>
          </div>

          <div class="col-sm-4">
            <div class="pb-1">Address</div>
          </div>

          <div class="col-sm-8">
            <div class="pb-1">Barangay Consolacion, Urdaneta City, Paangasinan</div>
          </div>

        </div>
      </div>

      <div class="about-section pt-4 px-3 px-lg-4 mt-1">
      <hr class="d-print-none"/>
      <div class=" px-3 px-lg-4">
      <h2 class="h3 mb-3">Professional Skills</h2>
        <div class="row">
          <div class="col-md-6">
            <div class="mb-2"><span>HTML</span>
              <div class="progress my-1">
                <div class="progress-bar bg-primary" role="progressbar" data-aos="zoom-in-right" data-aos-delay="100" data-aos-anchor=".skills-section" style="width: 90%" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
              </div>

            <div class="mb-2"><span>CSS</span>
              <div class="progress my-1">
                <div class="progress-bar bg-primary" role="progressbar" data-aos="zoom-in-right" data-aos-delay="200" data-aos-anchor=".skills-section" style="width: 85%" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
              </div>

            <div class="mb-2"><span>JavaScript</span>
              <div class="progress my-1">
                <div class="progress-bar bg-primary" role="progressbar" data-aos="zoom-in-right" data-aos-delay="300" data-aos-anchor=".skills-section" style="width: 75%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
              </div>
            </div>

            <div class="col-md-6">
              <div class="mb-2"><span>Adobe Photoshop</span>
                <div class="progress my-1">
                  <div class="progress-bar bg-primary" role="progressbar" data-aos="zoom-in-right" data-aos-delay="400" data-aos-anchor=".skills-section" style="width: 80%" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                </div>

            <div class="mb-2"><span>Sketch</span>
              <div class="progress my-1">
                <div class="progress-bar bg-primary" role="progressbar" data-aos="zoom-in-right" data-aos-delay="500" data-aos-anchor=".skills-section" style="width: 85%" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
              </div>

            <div class="mb-2"><span>Adobe XD</span>
              <div class="progress my-1">
                <div class="progress-bar bg-primary" role="progressbar" data-aos="zoom-in-right" data-aos-delay="600" data-aos-anchor=".skills-section" style="width: 75%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
              </div>
            <br>
          </div>
      </div>
    </div>
    <hr class="d-print-none"/>
    <div class="work-experience-section px-3 px-lg-4">
      <h2 class="h3 mb-4">Work Experience</h2>
      <div class="timeline">
        <div class="timeline-card timeline-card-primary card shadow-sm" style=" background-color: #33332c">
          <div class="card-body">
            <div class="h5 mb-1">On The Job Training<span class="text-muted h6"> at Studio 5</span></div>
            <div class="text-muted text-small mb-2">2019</div>
            <div>Nancayasan, Urdaneta City, Pangasinan</div>
          </div>
        </div>

        <div class="timeline-card timeline-card-primary card shadow-sm" style=" background-color:#33332c">
          <div class="card-body">
            <div class="h5 mb-1">Service Crew<span class="text-muted h6"> at Jollibee Magic Mall Urdaneta</span></div>
            <div class="text-muted text-small mb-2">2019</div>
            <div>Alexander Street, Urdaneta City, Pangasinan</div>
          </div>
        </div>
      </div>
      <br>
    </div>

    <hr class="d-print-none"/>
    <div class="page-break"></div>
    <div class="education-section px-3 px-lg-4 pb-4">
      <h2 class="h3 mb-4">Education</h2>
      <div class="timeline">
        <div class="timeline-card timeline-card-success card shadow-sm" style=" background-color: #33332c">
          <div class="card-body">
            <div class="h5 mb-1">Bachelor of Science in Information Technology <span class="text-muted h6">from Pangasinan State University - Urdaneta Campus</span></div>
            <div class="text-muted text-small mb-2">2019 - Present</div>
            <div>San Vicente, Urdaneta City, Pangasinan</div>
          </div>
        </div>

        <div class="timeline-card timeline-card-success card shadow-sm" style=" background-color: #33332c">
          <div class="card-body">
            <div class="h5 mb-1">Information Communication and Technology<span class="text-muted h6"> from Urdaneta City National High School</span></div>
            <div class="text-muted text-small mb-2">2017 - 2019</div>
            <div>San Vicente, Urdaneta City, Pangasinan</div>
          </div>
        </div>

      <div class="timeline-card timeline-card-success card shadow-sm" style=" background-color: #33332c">
        <div class="card-body">
          <div class="h5 mb-1">Regular Program<span class="text-muted h6"> from Urdaneta City National High School</span></div>
          <div class="text-muted text-small mb-2">2013 - 2017</div>
          <div>San Vicente, Urdaneta City, Pangasinan</div>
        </div>
      </div>
    </div>
  </div>
  </div>  
  </div>
  </div>
  </div>
  <br><br>
</body>
</html>