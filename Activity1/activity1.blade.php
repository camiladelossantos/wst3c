<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
         <title>Activity 1</title>
        <style>
            body {
            font-family: 'Nunito', sans-serif;
            }
            .head{
            background-color: white;
            padding: 2px;
            position: relative;
            left: 278px;
            top: 46px;
            z-index: 2;
            }
        </style>
    </head>
<body>
    <label class="head"><h6>Personal Information</h6></label>
    <div class="mx-auto" style = "max-width: 47rem; margin-top:25px;">
    <div class="card border-dark text-dark">
        <div class="card-body">

        <div class="col-sm-4 m-2">
            <label>Firstname:</label>
            <input type="text" class= "form-control" placeholder="Enter your firstname" name="fname">
        </div>
     
        <div class="col-sm-4 m-2">
            <label>Lastname:</label>
            <input type="text" class= "form-control" placeholder="Enter your lastname" name = "lname">
        </div>
      
        <div class = "col-sm-4 m-2">
            <label>Username:</label>
            <input type=" text" class =" form-control" placeholder = "Enter your username" name ="uname">
        </div>
    
        <div class="col-sm-4 m-2">
            <label>Password: </label>
            <input type="password" class="form-control" placeholder="Enter your password" name = "pass" style="border: #FF0000 solid 6px;">
        </div>
        <br>
        <div class="col-sm-6 m-2">
            <textarea class="form-control" style="overflow-y: scroll; padding-bottom: 55px;"></textarea>
        </div>
        <br>
        <div class="col-sm-4 m-2">
            <label>Birthdate:</label>
            <input type="date" class="form-select" placeholder="dd/mm/yyyy" style= "margin-top:-31px; margin-left: 80px;">
        <br>
            <label style="color:#FF0000;">Town: </label> 
            <select name="town" style= "margin-top:-31px; margin-left: 80px;" class="form-select">
                <option value="Nancayasan">Nancayasan</option>
                <option value="Camanang">Camanang</option> 
                <option value="Consolacion">Consolacion</option>
                <option value="Macalong">Macalong</option> 
                <option value="Sugcong">Sugcong</option>
            </select>
        <br>
            <label>Browser: </label>
            <input type="text" class="form-control" style= "margin-top:-31px; margin-left: 80px;">
        </div>
        
        <div class="col">
                <button class="btn btn-success" style="border: #FF0000 solid 3px; border-radius:1px;">Click me</button>
                <button style="border: solid 1px; border-radius:5px;">Reset</button>
                <button style="margin-left: 1px;border: solid 1px; border-radius:5px;">Pindot Me</button>
        </div>
  
        </div>
        </div>
    </div>
    </div>
<br><br>
</body>
</html>