<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <title>Order Details</title>
</head>
<body>
<div class="mx-auto" style = "max-width: 55rem; margin-top: 45px;">
    <div class="card border-dark " style=" background-color:">
    <div class="card-body">


    <div class="row"style="margin-left:30px; margin-right: 30px;">
        <label for="TransNo" class="col-sm-2 col-form-label">Trans No:</label>
    <div class="col-sm-10">
        <input type="text" readonly class="form-control" id="TransNo" value="<?php echo"{$TransNo}"?>">
    </div>
    </div>
<br>

<div class="row"style="margin-left:30px; margin-right: 30px;">
        <label for="OrderNo" class="col-sm-2 col-form-label">Order No:</label>
    <div class="col-sm-10">
        <input type="text" readonly class="form-control" id="OrderNo" value="<?php echo"{$OrderNo}"?>">
    </div>
    </div>
<br>

<div class="row"style="margin-left:30px; margin-right: 30px;">
        <label for="ItemID" class="col-sm-2 col-form-label">Item ID:</label>
    <div class="col-sm-10">
        <input type="text" readonly class="form-control" id="ItemID" value="<?php echo"{$ItemID}"?>">
    </div>
    </div>
<br>

  <div class="row"style="margin-left:30px; margin-right: 30px;">
        <label for="Name" class="col-sm-2 col-form-label">Name:</label>
    <div class="col-sm-10">
        <input type="text" readonly class="form-control" id="Name" value="<?php echo"{$Name}"?>">
    </div>
    </div>
<br>

<div class="row"style="margin-left:30px; margin-right: 30px;">
        <label for="Price" class="col-sm-2 col-form-label">Price:</label>
    <div class="col-sm-10">
        <input type="text" readonly class="form-control" id="Price" value="<?php echo"{$Price}"?>">
    </div>
    </div>
<br>

<div class="row"style="margin-left:30px; margin-right: 30px;">
        <label for="Qty" class="col-sm-2 col-form-label">Qty:</label>
    <div class="col-sm-10">
        <input type="text" readonly class="form-control" id="Qty" value="<?php echo"{$Qty}"?>">
    </div>
    </div>
<br>
    
</div>
</div>
</div>
<br>
<center>
<h3>Camila C. Delos Santos BSIT - 3C</h3>
</center>
</body>
</html>