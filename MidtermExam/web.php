<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

    Route::get('/item/{ItemNo}/{Name}/{Price}', function ($ItemNo, $Name, $Price) {
        return view('item', ['ItemNo'=>$ItemNo, 'Name'=>$Name, 'Price'=>$Price]);
    });
    Route::get('/customer/{CustomerID}/{Name}/{Address}', function ($CustomerID, $Name, $Address) {
        return view('customer', ['CustomerID'=>$CustomerID, 'Name'=>$Name, 'Address'=>$Address]);
    });
    Route::get('/order/{CustomerID}/{Name}/{OrderNo}/{Date}', function ($CustomerID, $Name, $OrderNo, $Date) {
        return view('order', ['CustomerID'=>$CustomerID, 'Name'=>$Name, 'OrderNo'=>$OrderNo, 'Date'=>$Date]);
    });
    Route::get('/orderDetails/{TransNo}/{OrderNo}/{ItemID}/{Name}/{Price}/{Qty}', function ($TransNo, $OrderNo, $ItemID, $Name, $Price, $Qty) {
        return view('orderDetails', ['TransNo'=>$TransNo, 'OrderNo'=>$OrderNo, 'ItemID'=>$ItemID, 'Name'=>$Name, 'Price'=>$Price, 'Qty'=>$Qty]);
    });
    