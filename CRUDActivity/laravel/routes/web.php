<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AppointmentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.register');
});

Route::group(['middleware' => ['auth']], function() {
    Route::get('/dashboard', 'App\Http\Controllers\DashboardController@index')->name
    ('dashboard');
});

Route::group(['middleware' => ['auth', 'role:user']], function() {
    Route::get('/dashboard/appointment', 'App\Http\Controllers\DashboardController@appointment')->name
    ('dashboard.appointment');
});

Route::group(['middleware' => ['auth', 'role:admin']], function() { 
    Route::get('/dashboard/adminlist', 'App\Http\Controllers\DashboardController@adminlist')->name
    ('dashboard.adminlist');
});


Route::resource('appointment', AppointmentController::class);

require __DIR__.'/auth.php';
