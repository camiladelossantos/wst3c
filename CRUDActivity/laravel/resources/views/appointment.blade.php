<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('School Appointment System') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                @extends('appointment.layout')
  
  @section('content')
  <div class="row">
      <div class="col-lg-12 margin-tb">
          <div class="pull-left">
              <h2>Add New Appointment</h2>
          </div>
          <div class="pull-right">
              <a class="btn btn-primary" href="{{ route('dashboard') }}"> Back</a>
          </div>
      </div>
  </div>
       
  @if ($errors->any())
      <div class="alert alert-danger">
          <strong>Whoops!</strong> There were some problems with your input.<br><br>
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
  @endif
       
  <form action="{{ route('appointment.store') }}" method="POST" enctype="multipart/form-data">
      @csrf
      
       <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="form-group">
                  <strong>Name:</strong>
                  <input type="text" name="name" class="form-control" placeholder="Name">
              </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="form-group">
                  <strong>Grade & Section:</strong>
                <input type="text" name="gs" class="form-control" placeholder="Grade & Section">
              </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="form-group">
                  <strong>Concern:</strong>
                  <textarea class="form-control" style="height:150px" name="concern" placeholder="Concern"></textarea>
              </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="form-group">
                  <strong>Image:</strong>
                  <input type="file" name="image" class="form-control" placeholder="image">
              </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                  <button type="submit" class="btn btn-primary">Submit</button>
          </div>
      </div>
       
  </form>
  @endsection
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
