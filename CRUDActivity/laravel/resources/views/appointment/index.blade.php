@extends('appointment.layout')
     
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <br>
                <center><h2>List of Appointment</h2> </center> <br>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('appointment.create') }}"> Create New Appointment</a>
            </div>
        </div>
    </div>
    
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
     
    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Image</th>
            <th>Name</th>
            <th>Grade & Section</th>
            <th>Concern</th>
            <th width="280px">Action</th>
        </tr>
        @foreach ($appointment as $appointments)
        <tr>
            <td>{{ ++$i }}</td>
            <td><img src="/image/{{ $appointments->image }}" width="100px"></td>
            <td>{{ $appointments->name }}</td>
            <td>{{ $appointments->gs }}</td>
            <td>{{ $appointments->concern }}</td>
            <td>
                <form action="{{ route('appointment.destroy',$appointments->id) }}" method="POST">
     
                    <a class="btn btn-info" href="{{ route('appointment.show',$appointments->id) }}">Show</a>
      
                    <a class="btn btn-primary" href="{{ route('appointment.edit',$appointments->id) }}">Edit</a>
     
                    @csrf
                    @method('DELETE')
        
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
    
    {!! $appointment->links() !!}
        
@endsection