<?php
    use Facebook\Exceptions\FacebookResponseException;
    use Facebook\Exceptions\FacebookSDKException;
    require_once(__DIR__.'/Facebook/autoload.php');

    define('APP_ID','1125333528254169');
    define('APP_SECRET','3e083b45c3a7aa61fddec871a55a1416');
    define('API_VERSION','v2.5');
    define('FB_BASE_URL','http://localhost/projects/repositories/wst3c/Login/fdashboard.php');
    if(!session_id()) {
        session_start();
    }
    $fb = new Facebook\Facebook([
        'app_id' => APP_ID,
        'app_secret' => APP_SECRET,
        'default_graph_version' => API_VERSION,
    ]);
    $fb_helper = $fb->getRedirectLoginHelper();
    try {
        if(isset($_SESSION['facebook_access_token'])) {
            $accessToken = $_SESSION['facebook_access_token'];}
        else {
            $accessToken = $fb_helper->getAccessToken();}
    } catch(FacebookResponseException $e) {
            echo 'Facebook API Error: ' . $e->getMessage();
            exit;
    } catch(FacebookSDKException $e) {
        echo 'Facebook SDK Error: ' . $e->getMessage();
            exit;
    }

    $permissions = ['email'];

    if (isset($accessToken)) {
    if (!isset($_SESSION['facebook_access_token'])) {
        $_SESSION['facebook_access_token'] = (string) $accessToken;
        $oAuth2Client = $fb->getOAuth2Client();
        $longLivedAccessToken = $oAuth2Client->getLongLivedAccessToken($_SESSION['facebook_access_token']);
        $_SESSION['facebook_access_token'] = (string) $longLivedAccessToken;
        $fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
        } 
        else {
            $fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
        }
        if (isset($_GET['code'])) {
            header("location: fdashboard.php");
        }
        try {
            $fb_response = $fb->get('/me?fields=name,first_name,last_name,email');		
            $fb_user = $fb_response->getGraphUser();
            $fb_response_picture = $fb->get('/me/picture?redirect=false&height=200');
            $picture = $fb_response_picture->getGraphUser();
            
            $_SESSION['fb_user_id'] = $fb_user->getField('id');
            $_SESSION['fb_user_name'] = $fb_user->getField('name');
            $_SESSION['fb_user_email'] = $fb_user->getField('email');
            $_SESSION['fb_user_pic'] = $picture['url'];
            
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            echo 'Facebook API Error: ' . $e->getMessage();
            session_destroy();
            header("location: index.php");
            exit;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            echo 'Facebook SDK Error: ' . $e->getMessage();
            exit;
        }
    } 
    else {	
        $fb_login_url = $fb_helper->getLoginUrl(FB_BASE_URL, $permissions);
    }
?>