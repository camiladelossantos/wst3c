<?php
    include('fconfig.php');
    if($_SESSION['facebook_access_token'] == '') {
    header("location: index.php");
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <title>Facebook</title>
    <style>
        a {
            color: white;
            font-weight: bold;
            text-decoration: none;
        }
        a:hover {
            color: white;
        }
    </style>
</head>
<body>
    <div class="mx-auto" style = "max-width: 30rem; margin-top: 100px;">
    <div class="card border-dark text-dark">
        <div class="card-body">
        <?php 
        $offset = strtotime("+7 hours");
        $date = date("m-d-Y");
        $time = date("h:i A", $offset);
        if(!empty($_SESSION['fb_user_pic'])) {
            echo "<center><img src = '$_SESSION[fb_user_pic]' height = '120px' width = '120px' style = 'margin-top:10px; margin-bottom: 10px;border: 1px solid; border-color: black;'></center>";
            }
        if(isset($_SESSION['fb_user_id'])) {
        if(!empty($_SESSION['fb_user_name'])) {
            echo "Name: " . $_SESSION['fb_user_name'];
            }
            }
            for($i = 0; $i < 21; $i++){
                echo "&ensp;";
                }
                echo "$date</br>";
                echo "Year and Section: BSIT-3C";
                for($i = 0; $i < 25; $i++){
                    echo "&ensp;";
                    }
                    echo "$time";
        ?>
        </div>
    </div><br>
    <div class="card border-dark text-white bg-dark">
        <div class ="card-body">
        <center>
            <h5>Welcome FB/GOOGLE</h5>
            <button class = "btn btn-danger"><a href="logout.php">LOGOUT</a></button>
        </center>
        </div>
    </div>
    </div>
</body>
</html>