<?php
    include('gconfig.php');
    require_once 'fconfig.php';
    if(!isset($_SESSION['access_token'])) {
        $google_login_btn = '<a href="'.$google_client->createAuthUrl().'"><button class = "btn btn-danger"><svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-google" viewBox="0 0 16 16">
        <path d="M15.545 6.558a9.42 9.42 0 0 1 .139 1.626c0 2.434-.87 4.492-2.384 5.885h.002C11.978 15.292 10.158 16 8 16A8 8 0 1 1 8 0a7.689 7.689 0 0 1 5.352 2.082l-2.284 2.284A4.347 4.347 0 0 0 8 3.166c-2.087 0-3.86 1.408-4.492 3.304a4.792 4.792 0 0 0 0 3.063h.003c.635 1.893 2.405 3.301 4.492 3.301 1.078 0 2.004-.276 2.722-.764h-.003a3.702 3.702 0 0 0 1.599-2.431H8v-3.08h7.545z"/>
        </svg>&ensp; &nbsp;&nbsp;&nbsp; Login with Google</a>';
    } 
    else {
        header("Location: gdashboard.php");
    }   
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <title>Login</title>
    <style>
        a {
            color: white;
            font-weight: bold;
            text-decoration: none;
        }
        a:hover {
            color: white;
        }
    </style>
</head>
<body>
    <?php if(isset($_SESSION['fb_user_id'])) {
        header("location: gdashboard.php");
    } 
    else {
    ?>
    <div class="mx-auto" style = "max-width: 30rem; margin-top: 100px;">
    <div class="card border-dark text-dark">
        <div class="card-body">
        <?php 
        $offset = strtotime("+7 hours");
        $date = date("m-d-Y");
        $time = date("h:i A", $offset);
        echo "Name: Camila C. Delos Santos"; 
        for($i = 0; $i < 19; $i++) {
            echo "&ensp;";
            }
            echo "$date</br>";
            echo "Year and Section: BSIT-3C";
            for($i = 0; $i < 25; $i++){
                echo "&ensp;";
                }
                echo "$time";
        ?>
    </div>
    </div><br>
    <div class="card border-dark text-white bg-dark" style = "height: 155px;">
    <div class="login-form"><br>
        <center><button class = "btn btn-primary"><svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-facebook" viewBox="0 0 16 16">
        <path d="M16 8.049c0-4.446-3.582-8.05-8-8.05C3.58 0-.002 3.603-.002 8.05c0 4.017 2.926 7.347 6.75 7.951v-5.625h-2.03V8.05H6.75V6.275c0-2.017 1.195-3.131 3.022-3.131.876 0 1.791.157 1.791.157v1.98h-1.009c-.993 0-1.303.621-1.303 1.258v1.51h2.218l-.354 2.326H9.25V16c3.824-.604 6.75-3.934 6.75-7.951z"/>
        </svg>&ensp; <a href="<?php echo $fb_login_url;?>">Login with Facebook</a></button></br></br>
            <?php
                echo '<div align="center">'.$google_login_btn . '</div>';
            ?>
        </button>
        </center>
    </div>
    </div>
    </div>
     <?php } ?>
</body>
</html>